package com.langufriend.api.common.model.db;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity(name = "word")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Builder
public class WordEntity {

	@Id
	@GeneratedValue
	private Long id;

	private String value;

	private String translation;

	private Long page;

	@Enumerated(EnumType.STRING)
	private WordStatus wordStatus;
}
