package com.langufriend.api.common.model.db;

public enum WordStatus {
    NEW,
    LEARNED
}
