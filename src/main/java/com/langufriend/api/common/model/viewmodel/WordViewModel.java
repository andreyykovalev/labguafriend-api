package com.langufriend.api.common.model.viewmodel;

import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class WordViewModel {
	private Long id;
	private String value;
	private String translation;
	private Set<Long> pages;
	private Long occurrences;
	private String status;

	public static void main(String[] args) {
		String test = "Drüben";
		String test2 = "Drüben";

		System.out.println(test.equals(test2));
	}

	public void addPage(Long page) {
		if (page == null) {
			return;
		}
		pages.add(page);
	}

	@Override
	public String toString() {
		return new Gson().toJson(this);
	}
}
