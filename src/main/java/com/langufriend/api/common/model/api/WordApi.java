package com.langufriend.api.common.model.api;

import lombok.*;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class WordApi {
	private Long id;
	private String value;
	private String translation;
	private Long page;
	private WordApiStatus status;

	public WordApi(String value, String translation, Long page, WordApiStatus status) {
		this.value = value;
		this.translation = translation;
		this.page = page;
		this.status = status;
	}
}
