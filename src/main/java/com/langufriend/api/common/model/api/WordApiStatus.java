package com.langufriend.api.common.model.api;

public enum WordApiStatus {
    NEW,
    LEARNED
}
