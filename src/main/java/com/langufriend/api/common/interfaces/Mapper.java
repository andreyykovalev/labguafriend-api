package com.langufriend.api.common.interfaces;

import java.util.List;

public interface Mapper<E, D> {
	D toDomain(E dto);

	E toEntity(D e);

	List<D> toDomain(Iterable<? extends E> dto);

	Iterable<E> toEntity(List<? extends D> dto);
}
