package com.langufriend.api.file;

import com.langufriend.api.common.model.viewmodel.WordViewModel;
import com.langufriend.api.word.WordsService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/files")
public class FileController {

    private final WordsService wordsService;
    private final FileService fileService;

    public FileController(WordsService wordsService,
                          FileService fileService) {
        this.wordsService = wordsService;
        this.fileService = fileService;
    }

    @PostMapping
    public ResponseEntity<?> writeToFileFromDb(@RequestParam("path") String path) {
        List<WordViewModel> words = wordsService.findAll();

        List<String> data = words.stream()
                .map(word -> word.getValue() + " - " + word.getTranslation())
                .collect(Collectors.toList());

        try {
            fileService.writeToFile(path, data);
            return ResponseEntity.noContent().build();
        } catch (WritingToFileException e) {
            return ResponseEntity.status(500).build();
        }
    }
}
