package com.langufriend.api.file;

import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.List;

@Service
public class FileService {

    public void writeToFile(String file, List<String> data) throws WritingToFileException {
        File requestedFile = new File(file);
        try {
            createFileIfNotExist(requestedFile);
            doWrite(requestedFile, data);
        } catch (IOException e){
            throw new WritingToFileException("Error writing data to a file");
        }
    }

    private void createFileIfNotExist(File requestedFile) throws IOException, WritingToFileException {
        if (!requestedFile.exists()) {
            boolean isFileCreated = requestedFile.createNewFile();
            if (!isFileCreated) {
                throw new WritingToFileException("New file isn't created");
            }
        }
    }

    private void doWrite(File requestedFile, List<String> data) throws IOException {
        Files.write(requestedFile.toPath(), data, StandardOpenOption.APPEND);
    }
}
