package com.langufriend.api.file;

public class WritingToFileException extends Exception {
    private String message;

    public WritingToFileException() {
        this.message = "File can't be created";
    }

    public WritingToFileException(String message) {
        super(message);
    }
}
