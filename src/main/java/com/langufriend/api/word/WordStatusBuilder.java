package com.langufriend.api.word;

import com.langufriend.api.common.model.api.WordApiStatus;
import com.langufriend.api.common.model.db.WordStatus;
import org.springframework.stereotype.Service;

@Service
public class WordStatusBuilder {
    public WordApiStatus buildWordApiStatus(WordStatus wordStatus) {
        if(wordStatus == null){
            return WordApiStatus.NEW;
        }
        switch (wordStatus) {
            case LEARNED:
                return WordApiStatus.LEARNED;
            case NEW:
            default:
                return WordApiStatus.NEW;
        }
    }

    public WordStatus buildWordStatus(WordApiStatus wordApiStatus) {
        switch (wordApiStatus) {
            case LEARNED:
                return WordStatus.LEARNED;
            case NEW:
            default:
                return WordStatus.NEW;
        }
    }

    public WordStatus buildWordStatus(String wordStatus) {
        switch (wordStatus) {
            case "LEARNED":
                return WordStatus.LEARNED;
            case "NEW":
            default:
                return WordStatus.NEW;
        }
    }
}
