package com.langufriend.api.word;

import com.langufriend.api.common.model.db.WordEntity;
import com.langufriend.api.common.model.db.WordStatus;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
interface WordsRepository extends PagingAndSortingRepository<WordEntity, Long> {

	default List<WordEntity> findAllWords() {
		final List<WordEntity> studentList = new ArrayList<>();

		Iterable<WordEntity> iterable = findAll();
		iterable.forEach(studentList::add);

		return studentList;
	}

	List<WordEntity> findAllByWordStatus(WordStatus wordStatus);

	List<WordEntity> findAllByValueIgnoreCase(String value);

	List<WordEntity> findAllById(Long id);
}
