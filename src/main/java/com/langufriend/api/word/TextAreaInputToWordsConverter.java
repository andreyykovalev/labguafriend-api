package com.langufriend.api.word;

import com.langufriend.api.common.model.api.WordApi;
import com.langufriend.api.common.model.api.WordApiStatus;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TextAreaInputToWordsConverter {

    public List<WordApi> convertTextAreaInputToWordsApi(String input) {
        String[] pairs = input.split("\r\n");
        return Arrays.stream(pairs).map(this::convertPairToWordApi).collect(Collectors.toList());
    }

    private WordApi convertPairToWordApi(String pair) {
        String[] pairAsArray = pair.split("-");
        String word = pairAsArray[0].trim();
        String translation = pairAsArray[1].trim();

        return new WordApi(word, translation, 1L, WordApiStatus.NEW);
    }
}
