package com.langufriend.api.word.responses;

import com.langufriend.api.common.model.viewmodel.WordViewModel;
import lombok.*;

import java.util.List;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FetchAllWordsResponse {
    private List<WordViewModel> words;
}
