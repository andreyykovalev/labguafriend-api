package com.langufriend.api.word.formdata;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
public class WordEditForm {
    private Long id;
    private String value;
}
