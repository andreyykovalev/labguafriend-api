package com.langufriend.api.word.occurences;

import com.langufriend.api.common.model.api.WordApi;
import com.langufriend.api.common.model.api.WordApiStatus;
import com.langufriend.api.common.model.viewmodel.WordViewModel;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

@Service
public class OccurrencesService {

	public List<WordViewModel> calculateOccurrences(List<WordApi> words) {
		Map<String, WordViewModel> wordsToOccurrences = new HashMap<>();

		words.forEach(word -> {
			if (wordsToOccurrences.containsKey(word.getValue())) {
				WordViewModel wordViewModel = wordsToOccurrences.get(word.getValue());
				wordViewModel.setId(word.getId());
				wordViewModel.setOccurrences(wordViewModel.getOccurrences() + 1);
				wordViewModel.setTranslation(buildTranslation(word, wordViewModel));
				wordViewModel.setStatus(buildStatus(word));
				wordViewModel.addPage(word.getPage());
			} else {
				HashSet<Long> pages = new HashSet<>();
				pages.add(word.getPage());
				WordViewModel viewModel = new WordViewModel(word.getId(), word.getValue(), word.getTranslation(), pages, 1L, buildStatus(word));
				wordsToOccurrences.put(word.getValue(), viewModel);
			}
		});

		return new ArrayList<>(wordsToOccurrences.values());

	}

	private String buildTranslation(WordApi w, WordViewModel viewModel) {
		String currentValue = viewModel.getTranslation();
		String anotherTranslation = w.getTranslation();

		if(currentValue.equalsIgnoreCase(anotherTranslation) || currentValue.contains(anotherTranslation)){
			return currentValue;
		}

		return currentValue + ", " + anotherTranslation;
	}

	private String buildStatus(WordApi word) {
		WordApiStatus status = word.getStatus();
		if (status == null) {
			return "";
		}
		String statusValue = status.name();
		String firstLetter = statusValue.substring(0, 1);
		String restOfLetters = statusValue.substring(1);
		return firstLetter + restOfLetters.toLowerCase();
	}
}
