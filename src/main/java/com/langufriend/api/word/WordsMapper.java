package com.langufriend.api.word;

import com.langufriend.api.common.interfaces.Mapper;
import com.langufriend.api.common.model.api.WordApi;
import com.langufriend.api.common.model.db.WordEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class WordsMapper implements Mapper<WordEntity, WordApi> {

	private final WordStatusBuilder wordStatusBuilder;

	@Autowired
	public WordsMapper(WordStatusBuilder wordStatusBuilder) {
		this.wordStatusBuilder = wordStatusBuilder;
	}

	@Override
	public WordApi toDomain(WordEntity dto) {
		return WordApi.builder()
			.id(dto.getId())
			.page(dto.getPage())
			.translation(dto.getTranslation())
			.value(dto.getValue().toLowerCase())
			.status(wordStatusBuilder.buildWordApiStatus(dto.getWordStatus()))
			.build();
	}

	@Override
	public WordEntity toEntity(WordApi e) {
		return WordEntity.builder()
			.id(e.getId())
			.page(e.getPage())
			.translation(e.getTranslation())
			.value(e.getValue().toLowerCase())
			.wordStatus(wordStatusBuilder.buildWordStatus(e.getStatus()))
			.build();
	}

	@Override
	public List<WordApi> toDomain(Iterable<? extends WordEntity> dto) {
		List<WordApi> wordsDto = new ArrayList<>();
		dto.iterator().forEachRemaining(e -> wordsDto.add(toDomain(e)));
		return wordsDto;
	}

	@Override
	public Iterable<WordEntity> toEntity(List<? extends WordApi> dto) {
		return dto.stream().map(this::toEntity).collect(Collectors.toList());
	}
}
