package com.langufriend.api.word;


import com.langufriend.api.common.model.api.WordApiStatus;
import lombok.*;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UpdateWordRequest {
    private String value;
    private WordApiStatus status;
}
