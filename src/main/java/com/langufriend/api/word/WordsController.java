package com.langufriend.api.word;

import com.langufriend.api.common.model.api.WordApi;
import com.langufriend.api.word.formdata.WordEditForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

@Controller
@RequestMapping("/words")
public class WordsController {

    private final WordsService wordsService;

    @Autowired
    public WordsController(WordsService wordsService) {
        this.wordsService = wordsService;
    }

    @GetMapping
    public String fetchAllWords(Model model) {
        model.addAttribute("words", wordsService.findAll());
        model.addAttribute("word",WordApi.builder().build());
        return "words";
    }

    @GetMapping("/filtered/{status}")
    public String fetchWordsByStatus(Model model, @PathVariable(name = "status") String status) {
        model.addAttribute("words", wordsService.findAllByFilter(status.toUpperCase()));
        model.addAttribute("word", WordApi.builder().build());
        return "words";
    }

    @PostMapping
    public String saveWord(@ModelAttribute WordApi api, Model model){
        wordsService.save(api);
        return "redirect:words";
    }

    @PostMapping(value = "/saveAll")
    public RedirectView handleForm(@RequestParam("words") String input) {
        wordsService.saveAll(input);
        return new RedirectView("/words");
    }

    @GetMapping("/{id}")
    public String fetchWord (Model model, @PathVariable(name = "id") Long id) {
        WordApi wordDb = wordsService.findById(id);

        WordEditForm wordEditForm = new WordEditForm();
        wordEditForm.setValue(wordDb.getValue());
        wordEditForm.setId(id);

        model.addAttribute("wordEditForm", wordEditForm);
        model.addAttribute("word", wordDb);
        return "word";
    }

    @PostMapping("/edit")
    public String editWord(@ModelAttribute WordEditForm wordEditForm, Model model){
        wordsService.update(wordEditForm);
        return "redirect:/words";
    }
}
