package com.langufriend.api.word;

import com.langufriend.api.common.model.viewmodel.WordViewModel;
import com.langufriend.api.word.responses.FetchAllWordsResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/words")
public class WordsRestService {

    private final WordsService wordsService;

    public WordsRestService(WordsService wordsService) {
        this.wordsService = wordsService;
    }

    @PutMapping
    public ResponseEntity<?> updateStatus(@RequestBody UpdateWordRequest updateWordRequest) {

        wordsService.updateWord(updateWordRequest);
        return ResponseEntity.noContent().build();
    }

    @GetMapping
    public ResponseEntity<FetchAllWordsResponse> fetchWords() {
        FetchAllWordsResponse response = FetchAllWordsResponse.builder()
                .words(wordsService.findAll())
                .build();
        return ResponseEntity.ok(response);
    }
}
