package com.langufriend.api.word;

import com.langufriend.api.common.model.api.WordApi;
import com.langufriend.api.common.model.db.WordEntity;
import com.langufriend.api.common.model.viewmodel.WordViewModel;
import com.langufriend.api.word.formdata.WordEditForm;
import com.langufriend.api.word.occurences.OccurrencesService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class WordsService {
	private final TextAreaInputToWordsConverter inputToWordsConverter;
	private final OccurrencesService occurrencesService;
	private final WordsRepository wordsRepository;
	private final WordsMapper wordsMapper;
	private final WordStatusBuilder wordStatusBuilder;

	public WordsService(TextAreaInputToWordsConverter inputToWordsConverter,
						OccurrencesService occurrencesService,
						WordsRepository wordsRepository,
						WordsMapper wordsMapper,
						WordStatusBuilder wordStatusBuilder) {
		this.inputToWordsConverter = inputToWordsConverter;
		this.occurrencesService = occurrencesService;
		this.wordsRepository = wordsRepository;
		this.wordsMapper = wordsMapper;
		this.wordStatusBuilder = wordStatusBuilder;
	}

	public List<WordViewModel> findAll(){
		List<WordApi> words = wordsMapper.toDomain(wordsRepository.findAllWords());
		return occurrencesService.calculateOccurrences(words);
	}

	public List<WordViewModel> findAllByFilter(String filter) {
		List<WordApi> words = wordsMapper.toDomain(wordsRepository.findAllByWordStatus(wordStatusBuilder.buildWordStatus(filter)));
		return occurrencesService.calculateOccurrences(words);
	}

	public WordApi save(WordApi word) {
		WordEntity wordEntity = wordsRepository.save(wordsMapper.toEntity(word));
		return wordsMapper.toDomain(wordEntity);
	}

	public List<WordApi> saveAll(List<WordApi> words) {
		Iterable<WordEntity> wordEntities = wordsRepository.saveAll(wordsMapper.toEntity(words));
		return wordsMapper.toDomain(wordEntities);
	}

	public List<WordApi> saveAll(String textAreaInput) {
		List<WordApi> wordsApi = inputToWordsConverter.convertTextAreaInputToWordsApi(textAreaInput);
		return saveAll(wordsApi);
	}

	public void updateWord(UpdateWordRequest updateWordRequest) {
		List<WordEntity> words = wordsRepository.findAllByValueIgnoreCase(updateWordRequest.getValue());
		List<WordEntity> wordEntities = words
				.stream()
				.map(w -> buildWordEntityWithNewStatus(updateWordRequest, w))
				.collect(Collectors.toList());
		wordsRepository.saveAll(wordEntities);
	}

	public WordApi findById(Long id) {
		Optional<WordEntity> wordEntity = wordsRepository.findById(id);
		return wordEntity.map(wordsMapper::toDomain).orElse(null);
	}

	public void update(WordEditForm wordEditForm) {
		WordApi requestedWord = findById(wordEditForm.getId());
		requestedWord.setValue(wordEditForm.getValue());
		save(requestedWord);
	}

	private WordEntity buildWordEntityWithNewStatus(UpdateWordRequest updateWordRequest, WordEntity w) {
		return WordEntity.builder()
				.id(w.getId())
				.page(w.getPage())
				.translation(w.getTranslation())
				.value(w.getValue())
				.wordStatus(wordStatusBuilder.buildWordStatus(updateWordRequest.getStatus()))
				.build();
	}
}
