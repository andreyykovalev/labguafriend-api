let table;

$(document).ready(function () {
    table = $('#all-words__table').DataTable({

        "bLengthChange": false,
        "searching": false,
        "bFilter": true,
        "bInfo": false,
        "bAutoWidth": false,
        "lengthChange": false,
        "pageLength": 15,
        select: {
            style: 'os',
            items: 'cell'
        },
        "columns": [
            null,
            null,
            null,
            null,
            null,
            {"orderDataType": "dom-select"}
        ],
        columnDefs: [
            { width: 100, targets: 0 },
            { width: 180, targets: 1 },
            { width: 100, targets: 2 },
            { width: 110, targets: 3 },
            { width: 70, targets: 4 },
            { width: 100, targets: 5 }
        ],
    });

    $.fn.dataTable.ext.order['dom-select'] = function (settings, col) {
        return this.api().column(col, {order: 'index'}).nodes().map(function (td, i) {
            return $('select', td).val();
        });
    }

});

function updateStatus(element , event, value) {

    let data = {
        "status": event.target.value,
        "value": value
    };
    $.ajax({
        url: '/api/words',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(data),
        type: 'PUT',
        dataType: 'json',
        success: function (responseData) {

        },
        error: function (e) {
            console.log(e)
            alert("Something went wrong. Please, try again later")
        }
    });
}

function openPronunciation(word) {
    window.open("https://forvo.com/word/" + word + "/#de");
}

function editWord(word) {
    const wordApi = JSON.parse(word)
    window.open('/words/' + wordApi.id, '_blank');
}
